package de.etamax.training.observations;

import static java.util.stream.Collectors.toMap;

import java.util.Optional;
import java.util.stream.Stream;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.springframework.stereotype.Component;

@Component
@Converter(autoApply = true)
public class StatusConverter implements AttributeConverter<Status, String> {

	@Override
	public String convertToDatabaseColumn(Status attribute) {
		return attribute.getId();
	}

	@Override
	public Status convertToEntityAttribute(String dbData) {
		var map = Stream.of(Status.values()).collect(toMap(Status::getId, e -> e));
		return Optional.ofNullable(map.get(dbData))
				.orElseThrow(() -> new StatusIdNotFoundException("id [" + dbData + "] not understood."));
	}
}
