package de.etamax.training.observations;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import de.etamax.training.domain.Instrument;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ObservationBody {
	@NotBlank
	private String name;
	@NotNull
	private Status status = Status.PENDING;
	@Valid
	@NotNull
	private Instrument instrument;
}
