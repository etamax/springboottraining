package de.etamax.training.observations;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping(path = "/observations")
public class ObservationsController {

	private final ObservationsService observationService;
	
	@GetMapping
	public List<Observation> findAllObservations() {
		return observationService.findAll();
	}
	
	@GetMapping(path = "/{id}")
	public Observation findObservationById(@PathVariable long id) {
		return observationService.findById(id);
	}

	@PostMapping
	public ResponseEntity<Observation> createObservation(@Valid @RequestBody ObservationBody observation) {
		var saved = observationService.save(observation);
		var location = ServletUriComponentsBuilder.fromCurrentRequestUri()
				.path("/{id}")
				.buildAndExpand(saved.getId())
				.toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping(path = "/{id}")
	public void updateObservation(@PathVariable long id, 
			@Valid @RequestBody ObservationBody observationBody) {
		
		observationService.update(observationService.findById(id), observationBody);
	}
}
