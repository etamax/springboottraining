package de.etamax.training.observations;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import de.etamax.training.domain.RestMessage;

@RestControllerAdvice
public class ObservationsControllerAdvice {

	@ResponseStatus(code = HttpStatus.NOT_FOUND)
	@ExceptionHandler(ObservationNotFoundException.class)
	public RestMessage handleException(ObservationNotFoundException exception) {
		return RestMessage.builder()
				.withError(exception.getMessage())
				.build();
	}
}
