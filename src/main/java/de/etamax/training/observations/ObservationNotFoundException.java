package de.etamax.training.observations;

public class ObservationNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -4127936733239938679L;

	public ObservationNotFoundException(long id) {
		super("Observation with id [" + id + "] was not found.");
	}
}
