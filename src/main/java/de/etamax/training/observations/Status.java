package de.etamax.training.observations;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public enum Status {
	PENDING("P"),
	STARTED("S"),
	EXECUTED("X"),
	ABORTED("A"),
	TERMINATED("T");
	
	private final String id;
}
