package de.etamax.training.observations;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import de.etamax.training.domain.Instrument;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "observations")
public class Observation {

	@Id
	@GeneratedValue
	private long id;
	
	@NotBlank
	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
	private Status status;
	
	@Valid
	@Column(nullable = false)
	private Instrument instrument;
}
