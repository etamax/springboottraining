package de.etamax.training.observations;

import org.springframework.data.jpa.domain.Specification;

import de.etamax.training.domain.Instrument;

public class ObservationSpecification {

	public static Specification<Observation> observationById(long id) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("id"), id);
	}
	
	public static Specification<Observation> observationByStatus(Status status) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("status"), status);
	}
	
	public static Specification<Observation> observationByInstrument(Instrument instrument) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("instrument"), instrument);
	}
}
