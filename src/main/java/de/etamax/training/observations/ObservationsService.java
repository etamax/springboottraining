package de.etamax.training.observations;

import java.util.List;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class ObservationsService {
	private final ObservationRepository repository;
	private final ObservationMapper mapper;

	public List<Observation> findAll() {
		return repository.findAll();
	}

	public Observation findById(long id) {
		return repository.findById(id)
				.orElseThrow(() -> new ObservationNotFoundException(id));
	}

	public Observation save(ObservationBody observation) {
		return repository.save(mapper.toObservation(observation));
	}

	public void update(Observation observation, ObservationBody observationBody) {
		mapper.updateObservationFields(observation, observationBody);
		repository.save(observation);
	}
}
