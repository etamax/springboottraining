package de.etamax.training.observations;

public class StatusIdNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 5415832319719698816L;

	public StatusIdNotFoundException(String message) {
		super(message);
	}
}
