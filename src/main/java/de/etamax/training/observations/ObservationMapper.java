package de.etamax.training.observations;

import org.springframework.stereotype.Component;

@Component
public class ObservationMapper {
	
	public Observation toObservation(ObservationBody observationBody) {
		var observation = new Observation();
		observation.setName(observationBody.getName());
		observation.setStatus(observationBody.getStatus());
		observation.setInstrument(observationBody.getInstrument());
		
		return observation;
	}

	public void updateObservationFields(Observation observation, ObservationBody observationBody) {
		observation.setInstrument(observationBody.getInstrument());
		observation.setName(observationBody.getName());
		observation.setStatus(observationBody.getStatus());
	}
}
