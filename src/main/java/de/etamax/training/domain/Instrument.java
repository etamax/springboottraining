package de.etamax.training.domain;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonValue;

import lombok.Value;

@Value
public class Instrument {
	@JsonValue
	@NotBlank
	String name;
}
