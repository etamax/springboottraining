package de.etamax.training.domain;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.springframework.stereotype.Component;

@Component
@Converter(autoApply = true)
public class InstrumentConverter implements AttributeConverter<Instrument, String> {

	@Override
	public String convertToDatabaseColumn(Instrument attribute) {
		return attribute.getName();
	}

	@Override
	public Instrument convertToEntityAttribute(String dbData) {
		return new Instrument(dbData);
	}
}
