package de.etamax.training.domain;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import lombok.Builder;
import lombok.Value;

@Value
@Builder(setterPrefix = "with")
public class RestMessage {
	String error;
	@Builder.Default
	ZonedDateTime timestamp = ZonedDateTime.now(ZoneId.of("UTC"));
}
