package de.etamax.training.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {
	@Pointcut("within(@org.springframework.stereotype.Repository *) "
			+ "|| within(@org.springframework.stereotype.Service *) "
			+ "|| within(@org.springframework.stereotype.Controller *) ")
	public void springBeanPointcut() {
		// Empty body
	}

	@Before("springBeanPointcut()")
	public void loggingMethod(JoinPoint joinPoint) {
		var logger = LoggerFactory.getLogger(joinPoint.getSignature().getDeclaringTypeName());
		logger.debug("Calling {}.{}()", joinPoint.getTarget().getClass(), joinPoint.getSignature().getName());
		if (joinPoint.getArgs().length != 0) {
			logger.trace("   with arguments ({})", joinPoint.getArgs());
		}
	}

	/**
	 * Aspect configuration for logging when throwing exceptions
	 *
	 * @param joinPoint jointPoint
	 * @param e         exception to be logged
	 */
	@AfterThrowing(pointcut = "springBeanPointcut()", throwing = "e")
	public void logAfterThrowing(JoinPoint joinPoint, Throwable e) {
		var logger = LoggerFactory.getLogger(joinPoint.getSignature().getDeclaringTypeName());
		logger.error("Exception with cause = {}", e.getCause() != null ? e.getCause() : "NULL");
	}
}
