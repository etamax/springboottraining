package de.etamax.training.common;

import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import de.etamax.training.domain.RestMessage;

@RestControllerAdvice
public class CommonControllerAdvice extends ResponseEntityExceptionHandler {

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		var errorMessage = ex.getAllErrors().stream()
				.map(CommonControllerAdvice::validationErrorToString)
				.collect(Collectors.joining(", "));
		var restMessage = RestMessage.builder().withError(errorMessage).build();

		return ResponseEntity.badRequest().body(restMessage);
	}
	
	private static String validationErrorToString(ObjectError error) {
		return String.format("[%s] %s.", ((FieldError)error).getField(), error.getDefaultMessage());
	}
}
