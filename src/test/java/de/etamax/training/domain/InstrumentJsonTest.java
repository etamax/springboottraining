package de.etamax.training.domain;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;

@JsonTest
class InstrumentJsonTest {
	@Autowired
	JacksonTester<Instrument> jacksonTester;

	@Test
	void serialize() throws IOException {
		var instrument = new Instrument("Spectroscopy");

		assertThat(jacksonTester.write(instrument)).isEqualToJson("\"Spectroscopy\"");
	}
}
