package de.etamax.training.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class InstrumentConverterTest {
	private InstrumentConverter converter = new InstrumentConverter();

	@Test
	void convertToDatabaseColumn() {
		assertThat(converter.convertToDatabaseColumn(new Instrument("INST"))).isEqualTo("INST");
	}

	@Test
	void convertToEntityAttribute() {
		assertThat(converter.convertToEntityAttribute("INST")).isEqualTo(new Instrument("INST"));
	}
}
