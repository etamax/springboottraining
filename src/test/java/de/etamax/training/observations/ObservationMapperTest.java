package de.etamax.training.observations;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import de.etamax.training.domain.Instrument;

class ObservationMapperTest {
	private ObservationMapper mapper = new ObservationMapper();

	@Test
	void toObservation() {
		var instrument = new Instrument("INST");
		var observationBody = new ObservationBody();
		observationBody.setInstrument(instrument);
		observationBody.setName("New observation");
		observationBody.setStatus(Status.EXECUTED);
		
		var observation = mapper.toObservation(observationBody);
		
		assertThat(observation.getStatus()).isEqualTo(Status.EXECUTED);
		assertThat(observation.getName()).isEqualTo("New observation");
		assertThat(observation.getInstrument()).isEqualTo(instrument);
	}
	
	@Test
	void updateObservationFields() {
		var observation = new Observation();
		observation.setId(105L);
		var observationBody = new ObservationBody();
		observationBody.setName("New name");
		observationBody.setStatus(Status.STARTED);
		observationBody.setInstrument(new Instrument("INST"));
		
		mapper.updateObservationFields(observation, observationBody);
		
		assertThat(observation.getId()).isEqualTo(105L);
		assertThat(observation.getName()).isEqualTo(observationBody.getName());
		assertThat(observation.getStatus()).isEqualTo(observationBody.getStatus());
		assertThat(observation.getInstrument()).isEqualTo(observationBody.getInstrument());
	}
}
