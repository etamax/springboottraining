package de.etamax.training.observations;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class StatusConverterTest {
	private StatusConverter converter = new StatusConverter();

	@ParameterizedTest
	@CsvSource({ "PENDING,P", "STARTED,S", "EXECUTED,X", "ABORTED,A", "TERMINATED,T" })
	void convertToDatabaseColumn(Status status, String id) {
		assertThat(converter.convertToDatabaseColumn(status)).isEqualTo(id);
	}

	@ParameterizedTest
	@CsvSource({ "P,PENDING", "S,STARTED", "X,EXECUTED", "A,ABORTED", "T,TERMINATED" })
	void convertToEntityAttribute(String id, Status status) {
		assertThat(converter.convertToEntityAttribute(id)).isEqualTo(status);
	}

	@Test
	void convertToEntityAttributeWithUnknownId() {
		RuntimeException exception = Assertions.assertThrows(StatusIdNotFoundException.class, () -> {
			converter.convertToEntityAttribute("J");
		});
		assertThat(exception.getMessage()).isEqualTo("id [J] not understood.");
	}
}
