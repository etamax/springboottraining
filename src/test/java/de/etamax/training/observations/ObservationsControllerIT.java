package de.etamax.training.observations;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import de.etamax.training.domain.Instrument;

@WebMvcTest(controllers = { ObservationsController.class, ObservationsControllerAdvice.class})
class ObservationsControllerIT {
	@Autowired
	MockMvc mockMvc;

	@MockBean
	ObservationsService service;

	@Test
	void findAllObservations() throws Exception {
		when(service.findAll()).thenReturn(List.of(new Observation()));

		mockMvc.perform(get("/observations")).andExpect(status().isOk());
	}

	@Test
	void findObservation() throws Exception {
		var observation = new Observation();
		observation.setId(15L);
		observation.setName("New observation");
		observation.setStatus(Status.PENDING);
		observation.setInstrument(new Instrument("INST"));

		when(service.findById(anyLong())).thenReturn(observation);

		mockMvc.perform(get("/observations/{id}", 15L)).andExpect(status().isOk())
				.andExpect(jsonPath("$.name", equalTo("New observation")))
				.andExpect(jsonPath("$.status", equalTo("PENDING"))).andExpect(jsonPath("$.id", equalTo(15)))
				.andExpect(jsonPath("$.instrument", equalTo("INST")));
	}

	@Test
	void findObservationButNotFound() throws Exception {
		when(service.findById(anyLong())).thenThrow(new ObservationNotFoundException(15L));

		mockMvc.perform(get("/observations/{id}", 15L))
		.andDo(print())
		.andExpect(status().isNotFound())
		.andExpect(jsonPath("$.error", notNullValue()))
		.andExpect(jsonPath("$.timestamp", notNullValue()));
	}

	@Test
	void createObservation() throws Exception {
		var newObservation = new Observation();
		newObservation.setId(15);

		when(service.save(any(ObservationBody.class))).thenReturn(newObservation);

		mockMvc.perform(post("/observations").contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("{\"name\":\"New observation\",\"instrument\":\"X-RAY\"}")).andDo(print())
				.andExpect(status().isCreated())
				.andExpect(header().string("Location", "http://localhost/observations/15"));
	}

	@Test
	void createObservationWithNullName() throws Exception {
		mockMvc.perform(post("/observations").contentType(APPLICATION_JSON_VALUE).content("{\"instrument\":\"X-RAY\"}"))
				.andDo(print())
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.error", equalTo("[name] must not be blank.")));
	}

	@Test
	void createObservationWithEmptyName() throws Exception {
		mockMvc.perform(post("/observations").contentType(APPLICATION_JSON_VALUE)
				.content("{\"name\":\"\", \"instrument\":\"X-RAY\"}")).andDo(print())
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.error", equalTo("[name] must not be blank.")));
	}

	@Test
	void createObservationWithInstrumentName() throws Exception {
		mockMvc.perform(post("/observations").contentType(APPLICATION_JSON_VALUE).content("{\"instrument\":\"X-RAY\"}"))
				.andDo(print()).andExpect(status().isBadRequest());
	}

	@Test
	void createObservationWithNullInstrument() throws Exception {
		mockMvc.perform(post("/observations").contentType(APPLICATION_JSON_VALUE)
				.content("{\"name\":\"New observation\"}")).andDo(print())
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.error", equalTo("[instrument] must not be null.")));
	}

	@Test
	void createObservationWithEmptyInstrument() throws Exception {
		mockMvc.perform(post("/observations").contentType(APPLICATION_JSON_VALUE)
				.content("{\"name\":\"New observation\",\"instrument\":\"\"}")).andDo(print())
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.error", equalTo("[instrument.name] must not be blank.")));
	}
	
	@Test
	void updateObservation() throws Exception {
		when(service.findById(anyLong())).thenReturn(new Observation());
		
		mockMvc.perform(put("/observations/{id}", 15L)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("{\"name\":\"New observation name\", \"status\":\"EXECUTED\", \"instrument\":\"INST\"}")
				)
		.andExpect(status().isOk());
	}
}
