package de.etamax.training.observations;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@ExtendWith(MockitoExtension.class)
class ObservationsControllerTest {

	@InjectMocks
	ObservationsController controller;
	
	@Mock
	ObservationsService service;
	

	@Test
	void getAllObservations() {
		when(service.findAll())
			.thenReturn(List.of(new Observation()));

		assertThat(controller.findAllObservations()).isNotEmpty();
	}
	
	@Test
	void findById() {
		when(service.findById(anyLong())).thenReturn(new Observation());
		
		assertThat(controller.findObservationById(1L)).isNotNull();
	}
	
	@Test
	void createObservation() {
		var request = new MockHttpServletRequest();
		request.setScheme("http");
		request.setServerName("localhost");
		request.setServerPort(-1);
		request.setRequestURI("/observations");
		request.setContextPath("/observations");
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
		
		var observationBody = new ObservationBody();
		var observation = new Observation();
		observation.setId(15L);
		
		when(service.save(any(ObservationBody.class))).thenReturn(observation);
		
		controller.createObservation(observationBody);
		
		verify(service).save(observationBody);
	}
	
	@Test
	void updateObservation() {
		
		var observationBody = new ObservationBody();
		var observation = new Observation();
		
		when(service.findById(anyLong())).thenReturn(observation );

		controller.updateObservation(15L, observationBody);
		
		verify(service).update(eq(observation), eq(observationBody));
	}
}
