package de.etamax.training.observations;

import static de.etamax.training.observations.ObservationSpecification.observationById;
import static de.etamax.training.observations.ObservationSpecification.observationByInstrument;
import static de.etamax.training.observations.ObservationSpecification.observationByStatus;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import com.github.database.rider.core.api.configuration.DBUnit;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.ExpectedDataSet;
import com.github.database.rider.spring.api.DBRider;

import de.etamax.training.domain.Instrument;

@DataJpaTest
@DBRider
@DBUnit(alwaysCleanBefore = true)
class ObservationRepositoryIT {
	@Autowired
	private ObservationRepository observationRepository;

	@Autowired
	TestEntityManager entityManager;

	@Test
	@DataSet(value = "findById.yml")
	void findById() {
		var observationOptional = observationRepository.findById(1L);

		assertThat(observationOptional).isPresent();

		var observation = observationOptional.get();
		assertThat(observation.getId()).isEqualTo(1L);
		assertThat(observation.getName()).isEqualTo("New observation");
		assertThat(observation.getStatus()).isEqualTo(Status.PENDING);
		assertThat(observation.getInstrument()).isEqualTo(new Instrument("X-RAY"));
	}

	@Test
	@DataSet(value = "findAll.yml")
	void findAll() {
		var observations = observationRepository.findAll();

		assertThat(observations).hasSize(10);
	}

	@Test
	@ExpectedDataSet(value = "saved.yml")
	void save() {
		var observation = new Observation();
		observation.setName("New observation");
		observation.setStatus(Status.EXECUTED);
		observation.setInstrument(new Instrument("INFRARED"));

		observationRepository.save(observation);

		entityManager.flush();
	}

	@Test
	@DataSet(value = "findAll.yml")
	void findByIdUsingSpecification() {
		var observationOptional = observationRepository.findOne(observationById(1L));

		assertThat(observationOptional).isPresent();

		var observation = observationOptional.get();
		assertThat(observation.getId()).isEqualTo(1L);
		assertThat(observation.getName()).isEqualTo("Observation #1");
		assertThat(observation.getStatus()).isEqualTo(Status.PENDING);
		assertThat(observation.getInstrument()).isEqualTo(new Instrument("X-RAY"));
	}

	@Test
	@DataSet(value = "findAll.yml")
	void findByStatusUsingSpecification() {
		var result = observationRepository.findAll(observationByStatus(Status.PENDING));

		assertThat(result).hasSize(2);

		assertThat(result.stream().map(e -> e.getId()).collect(Collectors.toList())).contains(1L, 6L);

	}

	@Test
	@DataSet(value = "findAll.yml")
	void findByInstrumentUsingSpecification() {
		var result = observationRepository.findAll(observationByInstrument(new Instrument("X-RAY")));

		assertThat(result).hasSize(5);

		assertThat(result.stream().map(e -> e.getId()).collect(Collectors.toList())).contains(1L, 2L, 3L, 4L, 5L);

	}

	@Test
	@DataSet(value = "findAll.yml")
	void findByInstrumentAndStatus() {
		var result = observationRepository
				.findAll(observationByInstrument(new Instrument("X-RAY")).and(observationByStatus(Status.TERMINATED)));

		assertThat(result).hasSize(1);

		assertThat(result.stream().map(e -> e.getId()).collect(Collectors.toList())).contains(5L);

	}

	@Test
	@DataSet(value = "findAll.yml")
	void findByInstrumentAndStatusOrId() {
		var result = observationRepository.findAll(observationById(7L)
				.or(observationByInstrument(new Instrument("X-RAY")).and(observationByStatus(Status.TERMINATED))));

		assertThat(result).hasSize(2);

		assertThat(result.stream().map(e -> e.getId()).collect(Collectors.toList())).contains(5L, 7L);
	}
}
