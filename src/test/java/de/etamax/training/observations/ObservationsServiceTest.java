package de.etamax.training.observations;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ObservationsServiceTest {
	@InjectMocks
	ObservationsService service;
	
	@Mock
	ObservationRepository repository;
	
	@Mock
	ObservationMapper mapper;

	@Test
	void findAll() {
		List<Observation> observations = Collections.emptyList();
		when(repository.findAll()).thenReturn(observations);
		
		assertThat(service.findAll()).isEqualTo(observations);
	}
	
	@Test
	void findById() {
		var observation = new Observation();
		when(repository.findById(anyLong())).thenReturn(Optional.of(observation));
		
		assertThat(service.findById(1L)).isEqualTo(observation);
	}
	
	@Test
	void findByIdButNotFound() {
		when(repository.findById(anyLong())).thenReturn(Optional.empty());
		
		var exception = assertThrows(ObservationNotFoundException.class, () -> service.findById(1L));
		
		assertThat(exception.getMessage()).isEqualTo("Observation with id [1] was not found.");
	}
	
	@Test
	void save() {
		var saved = new Observation();
		when(mapper.toObservation(any(ObservationBody.class))).thenReturn(new Observation());
		when(repository.save(any(Observation.class))).thenReturn(saved);
		
		assertThat(service.save(new ObservationBody())).isEqualTo(saved);
	}
	
	@Test
	void update() {
		service.update(new Observation(), new ObservationBody());
		
		verify(mapper).updateObservationFields(any(Observation.class), any(ObservationBody.class));
		verify(repository).save(any(Observation.class));
	}
}
