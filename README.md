# Training: Development with Spring/Spring Boot

## Building and running

* To build and test the application:

	$> ./mvnw clean test surefire-report:report jacoco:report

* To run the application:

	$> ./mvnw spring-boot:run


## References

* [lombok](https://projectlombok.org)
* [DBRider](https://github.com/database-rider/database-rider)

## Context and Domain

The example application is built using Spring Boot. The context is quite straightforward:

```plantuml
database db as "Database"

artifact SpringBootProject {

	artifact ccc as "Cross-Cutting concerns" {
		[Logging]
	}

	artifact requestFilter as "Filter/Interceptor" {
		[Filter]
		[Interceptor]
	}	
	
	package observations {
		requestFilter -down-> [controller]
	
		[controller] --> [service]
		[service] --> [data JPA]
		[data JPA] --> db
	}
	
	observations --> ccc 
}
```

The domain contained in the application is:

```plantuml
enum Status {
	PENDING
	STARTED
	EXECUTED
	ABORTED
	TERMINATED 
}

class Instrument <<Value Object>> {
	-name: String
}

class Observation {
	-id: long
	-name: String
}

Observation -left-> Status
Observation -down-> Instrument
```


## Request Filters

* `RequestCorrelationIdFilter`: Creates a unique ID for the request
* `LogRequestFilter`: Log the HTTP request

## Cross-concern

* `LoggingAspect`: Add logging when calling methods in `@Controller`, `@Service` or `@Repository` components.
